FROM centos:7

RUN yum install make -y 
RUN yum install gcc openssl-devel bzip2-devel libffi-devel zlib-devel -y

RUN curl https://www.python.org/ftp/python/3.8.13/Python-3.8.13.tgz --output Python-3.8.13.tgz 
RUN tar xzf Python-3.8.13.tgz 
RUN cd Python-3.8.13 && ./configure --enable-optimizations && make altinstall && \
    ln -sfn /usr/local/bin/python3.8 /usr/bin/python3.8 && \
    ln -sfn /usr/local/bin/pip3.8 /usr/bin/pip3.8 && \
    python3.8 -m pip install --upgrade pip

RUN pip3.8 install flask flask-restful flask-jsonpify
COPY python-api.py /python_api/python-api.py
CMD ["python3.8", "/python_api/python-api.py"]
